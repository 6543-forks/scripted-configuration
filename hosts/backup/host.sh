#!/usr/bin/env bash

set -ex

MEM=4
CPU=4
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/ceph-cluster/production mnt/ceph-cluster none defaults,bind,create=dir 0 0
lxc.mount.entry = / mnt/btrfs none ro,defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
