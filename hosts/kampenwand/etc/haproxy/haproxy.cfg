#####################################
## Global Configuration & Defaults ##
#####################################

global
  # log stderr local7

  maxconn 262144
  stats socket /var/run/haproxy/api.sock user haproxy group haproxy mode 660 level admin expose-fd listeners

  # generated 2021-06-05, Mozilla Guideline v5.6, HAProxy 2.1, OpenSSL 1.1.1d, intermediate configuration
  # https://ssl-config.mozilla.org/#server=haproxy&version=2.1&config=intermediate&openssl=1.1.1d&guideline=5.6
  ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
  ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
  ssl-default-bind-options prefer-client-ciphers no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

  ssl-default-server-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
  ssl-default-server-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
  ssl-default-server-options no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

  # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
  ssl-dh-param-file /etc/ssl/dhparam.pem

defaults
  log global
  timeout connect 30000
  timeout check 300000
  timeout client 300000
  timeout server 300000
  default-server init-addr last,libc,none

############################################################################
## Frontends: HTTP; HTTPS → HTTPS SNI-based; HTTPS → HTTP(S) header-based ##
############################################################################

frontend http_redirect_frontend
  # HTTP backend to redirect everything to HTTPS
  bind :::80 v4v6
  mode http
  http-request redirect scheme https

frontend ssh_frontend
  # TCP backend to forward SSH to Gitea
  bind :::22 v4v6
  mode tcp
  default_backend ssh_backend

backend ssh_backend
  server gitea_ssh_server gitea-production.lxc.local:22 no-check maxconn 40
  mode tcp

frontend ssh_frontend_forgejo_next
  bind :::2222 v4v6
  mode tcp
  default_backend ssh_backend_forgejo_next

backend ssh_backend_forgejo_next
  server forgejo_next_ssh_server forgejo-next.lxc.local:2222 no-check maxconn 8

frontend ssh_frontend_staging
  bind :::3222 v4v6
  mode tcp
  default_backend ssh_backend_staging

backend ssh_backend_staging
  server gitea_ssh_staging gitea-staging.lxc.local:22 no-check maxconn 2

frontend https_sni_frontend
  # TCP backend to forward to HTTPS backends based on SNI
  bind :::443 v4v6
  mode tcp

  # Wait up to 5s for a SNI header & only accept TLS connections
  tcp-request inspect-delay 5s
  tcp-request content capture req.ssl_sni len 255

  # Log SNI & corresponding backend
  # log-format "%ci:%cp -> %[capture.req.hdr(0)] @ %f (%fi:%fp) -> %b (%bi:%bp)"
  # tcp-request content accept if { req.ssl_hello_type 1 }

  ###################################################
  ## Rules: forward to HTTPS(S) header-based rules ##
  ###################################################
  acl use_http_backend req.ssl_sni -i "codeberg.org"
  acl use_http_backend req.ssl_sni -i "staging.codeberg.org"
  acl use_http_backend req.ssl_sni -i "www.codeberg.org"
  acl use_http_backend req.ssl_sni -i "about.codeberg.org"
  acl use_http_backend req.ssl_sni -i "join.codeberg.org"
  acl use_http_backend req.ssl_sni -i "polls.codeberg.org"
  acl use_http_backend req.ssl_sni -i "ci.codeberg.org"
  acl use_http_backend req.ssl_sni -i "translate.codeberg.org"
  acl use_http_backend req.ssl_sni -i "mcaptcha.codeberg.org"
  acl use_http_backend req.ssl_sni -i "monitor.codeberg.org"
  acl use_http_backend req.ssl_sni -i "admin.codeberg.org"
  acl use_http_backend req.ssl_sni -i "codeberg-test.org"
  acl use_http_backend req.ssl_sni -i "docs.codeberg.org"
  acl use_http_backend req.ssl_sni -i "design.codeberg.org"
  acl use_http_backend req.ssl_sni -i "fonts.codeberg.org"
  acl use_http_backend req.ssl_sni -i "blog.codeberg.org"
  acl use_http_backend req.ssl_sni -i "get-it-on.codeberg.org"
  acl use_http_backend req.ssl_sni -i "pages.codeberg.org"
  acl use_http_backend req.ssl_sni -i "forgejo-ci.codeberg.org"
  acl use_http_backend req.ssl_sni -i "forgejo-release-ci.codeberg.org"
  acl use_forgejo_next_backend req.ssl_sni -i "next.forgejo.org"
  use_backend https_termination_backend if use_http_backend
  use_backend forgejo_next_https_backend if use_forgejo_next_backend

  ############################
  ## Rules: HTTPS SNI-based ##
  ############################
  # use_backend xyz_backend if { req.ssl_sni -i "xyz" }
  default_backend pages_backend

frontend https_termination_frontend
  # Terminate TLS for HTTP backends
  bind /tmp/haproxy-tls-termination.sock accept-proxy ssl strict-sni alpn h2,http/1.1 crt /etc/ssl/private/haproxy/
  mode http

  ############################################
  ## Mitigations against malicious requests ##
  ############################################

  # Block known bad bot UAs
  tcp-request inspect-delay 15s
  acl bad-bot hdr_sub(user-agent) -f /etc/haproxy/forbidden-ua-list.txt
  tcp-request content reject if bad-bot

  # Send "429 Too Many Requests" for signup page (deprecated)
  #http-request track-sc0 src table per_ip_rates if { path /user/sing_up }
  #http-request deny deny_status 429 if { sc_http_req_rate(0) gt 100 }

  # Preserve path for backend
  http-request set-var(txn.path) path

  ##############################
  ## General request settings ##
  ##############################

  # Enable compression for backends which don't already do it
  compression algo gzip
  compression type application/javascript application/json image/svg+xml text/css text/html text/javascript text/plain text/xml

  # Force HTTPS through HSTS (63072000 seconds)
  http-response set-header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"

  # Opt out of FLoC explicitly
  http-response set-header Permissions-Policy "interest-cohort=()"

  # Only allow secure cookies, and don't allow cookies for CORS requests
  http-response replace-header Set-Cookie (.*) "\1; Secure; SameSite=Lax"

  # Cache avatars for 6 hours
  http-response set-header Cache-Control "private, max-age=21600" if { capture.req.uri -m beg /avatars/ }
  
  # Disable embedding in external frames
  http-response set-header X-Frame-Options "sameorigin"

  # Force Content-Type header to be respected, so e.g. raw links can't be used in <script> tags
  http-response set-header X-Content-Type-Options "nosniff"

  # Add CSP to avoid XSS attacks
  # Currently in Report-Only mode to detect if it theoretically would work
  # You can find a line-wrapped version (using Jinja2 comments) in the ansible template of this file
  # Disabled, as we do not track this and generates unnecessary messages in the browser console.
  # http-response set-header Content-Security-Policy-Report-Only "default-src data: 'self' https://*.codeberg.org https://codeberg.org; script-src 'self' https://*.codeberg.org https://codeberg.org; style-src data: 'self' 'unsafe-inline' https://*.codeberg.org https://codeberg.org; img-src *; media-src *; object-src 'none'"

  # Log Host header & corresponding backend
  # http-request capture req.hdr(Host) len 255
  # log-format "%ci:%cp -> %[capture.req.hdr(0)] @ %f (%fi:%fp) -> %b (%bi:%bp)"

  ##################################
  ## Rules: HTTPS(S) header-based ##
  ##################################

  redirect prefix https://codeberg.org code 301 if { hdr(host) -i www.codeberg.org }
#  redirect prefix https://about.codeberg.org code 301 if { hdr(host) -i join.codeberg.org }

  #use_backend csp_backend if { path -i /.well-known/csp-report }
  use_backend gitea_backend_production if { hdr(host) -i codeberg.org }
  use_backend backend_forgejo_migration if { hdr(host) -i staging.codeberg.org }
  use_backend gitea_backend_staging if { hdr(host) -i codeberg-test.org }
  use_backend join_backend if { hdr(host) -i join.codeberg.org }
  use_backend polls_backend if { hdr(host) -i polls.codeberg.org }
  use_backend ci_backend if { hdr(host) -i ci.codeberg.org }
  use_backend translate_backend if { hdr(host) -i translate.codeberg.org }
  use_backend mcaptcha_backend if { hdr(host) -i mcaptcha.codeberg.org }
  use_backend monitor_backend if { hdr(host) -i monitor.codeberg.org }
  use_backend admin_backend if { hdr(host) -i admin.codeberg.org }
  use_backend pages_redir_backend if { hdr(host) -i pages.codeberg.org }
  use_backend forgejo-ci_backend if { hdr(host) -i forgejo-ci.codeberg.org }
  use_backend forgejo-ci_backend if { hdr(host) -i forgejo-release-ci.codeberg.org }
  use_backend static_backend if { hdr(host) -i docs.codeberg.org }
  use_backend static_backend if { hdr(host) -i design.codeberg.org }
  use_backend static_backend if { hdr(host) -i get-it-on.codeberg.org }
  use_backend static_backend if { hdr(host) -i fonts.codeberg.org }
  use_backend static_backend if { hdr(host) -i blog.codeberg.org }
  default_backend pages_backend_http

backend https_termination_backend
  # Redirect to the terminating HTTPS frontend for all HTTP backends
  server https_termination_server /tmp/haproxy-tls-termination.sock send-proxy-v2-ssl-cn
  mode tcp

backend per_ip_rates
  stick-table type ip size 1m expire 15m store http_req_rate(15m)

###############################
## Backends: HTTPS SNI-based ##
###############################

backend forgejo_next_https_backend
  # The SSL certificate for forgejo-next is provided by Forgejo
  server forgejo_next_server forgejo-next.lxc.local:3000 no-check
  mode tcp

backend pages_backend
  # Pages server is a HTTP backend that uses its own certificates for custom domains
  server pages_server pages.lxc.local:443 no-check
  mode tcp

backend pages_backend_http

  stick-table type ipv6 size 1m expire 300s store http_req_rate(300s)
  http-request track-sc0 src
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 1800 } !{ src 10.0.3.0/24 }

  # When reusing an HTTP/2 connection, HAProxy's HTTP server is already active, so we need to have a HTTP backend as well
  server pages_server_http pages.lxc.local:443 no-check ssl verify none sni req.hdr(host)
  mode http

####################################
## Backends: HTTP(S) header-based ##
####################################

backend gitea_backend_production
  # attempt general rate limiting
  stick-table type ipv6 size 1m expire 24h store http_req_rate(360s),gpc0_rate(24h)
  http-request track-sc0 src

  acl fast_request path_beg /assets /avatars
  acl gitop path_sub commit branch compare
  acl bigrepo path_sub linux bsd kernel

  # rate limiting
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 2000 } !{ src -f /etc/haproxy/ratelimit-whitelist.acl }
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 400 } bigrepo !{ src -f /etc/haproxy/ratelimit-whitelist.acl }
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 1500 } gitop !{ src -f /etc/haproxy/ratelimit-whitelist.acl }
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 30 } { src -f /etc/haproxy/ratelimit-blacklist.acl } # manual intervention blacklist

  # Registration rate limiting
  http-request deny deny_status 429 if { sc0_gpc0_rate gt 1 } { method POST } { var(txn.path) -m beg /user/cbrgp/ }
  http-response sc-inc-gpc0(0) if { status 200 } { method POST } { var(txn.path) -m beg /user/cbrgp/ }

  # request priorities
  http-request set-priority-class int(-50) if fast_request
  http-request set-priority-class int(-30) if METH_POST
  http-request set-priority-class int(10) if { sc_http_req_rate(0) gt 500 } !{ src 10.0.3.0/24 }
  http-request set-priority-class int(20) if gitop
  http-request set-priority-class int(30) if { sc_http_req_rate(0) gt 1000 } !{ src 10.0.3.0/24 }
  http-request set-priority-class int(40) if bigrepo

  # Replace 500 response on POST to issue or pull request endpoints with 429
  http-response return status 429 default-errorfiles if { status 500 } { method POST } { var(txn.path) -m end /issues/new OR var(txn.path) -m end /comments OR var(txn.path) -m sub /compare/ }

  errorfile 429 /etc/haproxy/error-pages/429.http
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server gitea_server gitea-production.lxc.local:3000 no-check maxconn 350
  mode http

backend gitea_backend_staging
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server gitea_server gitea-staging.lxc.local:3000 no-check
  mode http

userlist UsersFor_staging
  user forgejo insecure-password ${HAPROXY_BASICAUTH_MIGRATION}

backend backend_forgejo_migration
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  acl AuthOk http_auth(UsersFor_staging)
  http-request auth realm UserAuth if !AuthOk
  server gitea_server forgejo-migration.lxc.local:3000 no-check
  mode http

backend ci_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server ci_server ci.lxc.local:8000 no-check
  mode http

backend forgejo-ci_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server forgejo-ci_server forgejo-ci.lxc.local:80 check send-proxy-v2
  mode http

backend translate_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  http-request set-header X-Forwarded-Proto https
  server translate_server translate.lxc.local:80 no-check
  mode http

backend mcaptcha_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server mcaptcha_server mcaptcha.lxc.local:7000 no-check
  mode http

userlist UsersFor_admin
  user admin insecure-password ${HAPROXY_BASICAUTH_ADMIN}

backend admin_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  acl AuthOk http_auth(UsersFor_admin)
  http-request auth realm UserAuth if !AuthOk
  server admin_server mcaptcha.lxc.local:7000 no-check
  mode http

userlist UsersFor_monitor
  user monitor insecure-password ${HAPROXY_BASICAUTH_MONITOR}

backend monitor_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  acl AuthOk http_auth(UsersFor_monitor)
  http-request auth realm UserAuth if !AuthOk
  server monitor_server 127.0.0.1:19999 no-check
  mode http

backend static_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server static_server static.lxc.local:80 no-check
  http-response set-header "Access-Control-Allow-Origin" "*"
  mode http

backend polls_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server static_server ev.lxc.local:82 no-check
  http-response set-header "Access-Control-Allow-Origin" "*" # is this necessary?
  mode http

backend join_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server static_server ev.lxc.local:5000 no-check
  mode http

backend pages_redir_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  http-request redirect code 301 location https://codeberg.page/ if { capture.req.uri = "/" }
  http-request redirect code 301 location https://%[capture.req.uri,regsub(^/,,),regsub(/.*$,,)].codeberg.page/%[capture.req.uri,regsub(^/[^/]+/?,,)]
  mode http

# TODO: move the errorfile & forwardfor options to a default section?
