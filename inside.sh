#!/usr/bin/env bash

set -ex

# define constants
HOSTNAME=$(hostname -s)
SCRIPT_LOCATION="/root/scripted-configuration.git"
cd "${SCRIPT_LOCATION}"

# install packages
apt-get update && apt-get upgrade -y
apt-get -y install sudo vim nano joe curl wget unzip git man-db htop iotop dnsutils tmux
apt-get -y install unattended-upgrades
# reduced from Ansible:
# software-properties-common systemd-journal-remote dma python3-pip python3-setuptools
# nethogs net-tools dnsutils psmisc nmon

git config --global user.email "contact@codeberg.org"
git config --global user.name "Codeberg Admins @ ${HOSTNAME}"

source base/base.sh

line_in_file /root/.bashrc "[ $(tput colors 2>/dev/null || echo 0) -le 1 ] || PS1='\\[\\e[1;38;5;244m\\]\\t \\[\\e[1;36m\\]\\u@\\H \\[\\e[1;33m\\]\\w \\[\\e[1;36m\\]\\$ \\[\\e[0m\\]'"

# execute more setups found via
# hosts/HOSTNAME.inside.sh or
# hosts/HOSTNAME/inside.sh

if [ -f "hosts/${HOSTNAME}.inside.sh" ]; then
	source "hosts/${HOSTNAME}.inside.sh"
fi
if [ -f "hosts/${HOSTNAME}/inside.sh" ]; then
	source "hosts/${HOSTNAME}/inside.sh"
fi
