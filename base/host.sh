#!/usr/bin/env bash

set -ex

function host_configure_container () {
	if [ -z "${CPU+x}" ]; then
		read -p "How many CPU cores should be made available? " CPU
		: ${CPU:=2}
	fi

	if [ -z "${MEM+x}" ]; then
		read -p "How much memory should be made available, in GB? " MEM
		: ${MEM:=2}
	fi

	MEM_MAX=$((1024**3 * ${MEM} * 6/5))
	MEM_HIGH=$((1024**3 * ${MEM}))
	CPU_MAX=$((100000 * ${CPU}))
	: ${VIRTUALIZATION:=lxc}
}

function setup_new_kvm () {
	virt-install --virt-type kvm --name "${HOSTNAME}" \
		--location http://deb.debian.org/debian/dists/bookworm/main/installer-amd64/ \
		--os-variant debiantesting \
		--memory $((MEM * 1024)) \
		--graphics none \
		--console pty,target_type=serial \
		--extra-args "console=ttyS0" \
		--vcpus ${CPU} \
		--disk size=100 \
		--network=default
	virsh autostart "${HOSTNAME}"
}

function setup_new_lxc () {
	LXCSTORAGE="/var/lib/lxc"

	if [ "$EUID" -ne 0 ]; then
		if ! confirm "You are executing this script as '$USER', containers will not be global, but user specific. Do you want to continue?"; then exit 1; fi
		echo "Running as User '$USER', checking requirements"
		userpasswd="$(getent passwd "$USER" || (echo >&2 "failed to get passwd entry for user $USER while resolving home directory" && exit 1))"
		USERHOME="$(echo "$userpasswd" | cut -d: -f6)"
		LXCSTORAGE="${USERHOME}/.local/share/lxc"
		LXCDEFAULTCONFIG="${USERHOME}/.config/lxc/default.conf"
		echo "Running in user mode, lxc-storage is '$LXCSTORAGE'"
		if ! [ -d "${LXCSTORAGE}" ] || ! [ -f "${LXCDEFAULTCONFIG}" ]; then
			echo "user based lxc not configured"
			exit 1
		fi
	fi

	# if container exists, stop for configuration, otherwise create
	if [ -d "${LXCSTORAGE}/${HOSTNAME}" ]; then
		lxc-stop "${HOSTNAME}" || if [ $? == 2 ]; then true; fi
	else
		lxc-create --name="${HOSTNAME}" --template=debian -- --release bookworm
		rsync . "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git" -aR
		git remote add "${HOSTNAME}" "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git"
	fi

	# clear per-service config from file
	sed -i '/# per-service config starts here/,$d' "${LXCSTORAGE}/${HOSTNAME}/config"
	envsubst <<EOF >> "${LXCSTORAGE}/${HOSTNAME}/config"

# per-service config starts here
# don't manually edit below, will be overriden
lxc.start.auto = 1
lxc.cgroup2.memory.high = ${MEM_HIGH}
lxc.cgroup2.memory.max = ${MEM_MAX}
lxc.cgroup2.cpu.max = ${CPU_MAX}
lxc.cgroup2.devices.allow = c 10:229 rwm
${LXC_CUSTOM}
EOF

	lxc-start "${HOSTNAME}"
	sleep 5 # wait for network to come up (workaround)

	if [ -f "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git/inside.sh" ]; then
		lxc-attach --name="${HOSTNAME}" -- /root/scripted-configuration.git/inside.sh
	fi
}
